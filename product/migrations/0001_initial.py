# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-15 07:04
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('code', models.CharField(max_length=10)),
                ('price', models.DecimalField(decimal_places=10, max_digits=19)),
                ('measure', models.CharField(max_length=10)),
            ],
        ),
    ]
