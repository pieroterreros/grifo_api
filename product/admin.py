# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Product

# Register your models here.

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
	list_display = ('uuid', 'name', 'code', 'price', 
					'measure',)
	search_fields = ['uuid', 'name', 'code', 'price',
					'measure',]
