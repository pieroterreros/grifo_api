# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from django.db import models

# Create your models here.

class Product(models.Model):
	uuid = models.UUIDField(
		primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(max_length=50)
	code = models.CharField(max_length=10)
	price = models.DecimalField(max_digits=19, decimal_places=2)
	measure = models.CharField(max_length=10)

	"""docstring for Product"""

	class Meta:
		verbose_name = u"Product"
		verbose_name_plural = "Products"

	def __unicode__(self):
		return '%s - %s' % (self.name,self.code)


	@classmethod
	def find(cls,code):
		product = cls.objects.filter(
			code=code)
		if product.exists():
			return product[0]
		return None
		