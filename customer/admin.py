# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Customer

# Register your models here.

# @admin.register(Person)
# class PersonAdmin(admin.ModelAdmin):
#     list_display = ('uuid', 'first_name', 'last_name',
#                     'email',)
#     search_fields = ['uuid', 'first_name', 'last_name',
#                     'email',]


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'ruc', 'address',
                    'first_name', 'last_name', 'email')
    search_fields = ['uuid', 'ruc', 'address',
                    'first_name', 'last_name', 'email']
