# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

import uuid

# Create your models here.

class Customer(models.Model):
	uuid = models.UUIDField(
		primary_key=True, default=uuid.uuid4, editable=False)
	first_name = models.CharField(max_length=60)
	last_name = models.CharField(max_length=60)
	email = models.EmailField(blank=True, null=True)
	ruc = models.CharField(max_length=11,blank=True, null=True)
	address = models.CharField(max_length=200)
	"""docstring for """

	class Meta:
		verbose_name = u"Customer"
		verbose_name_plural = "Customers"

	def __unicode__(self):
		return '%s %s ' % (self.first_name,self.last_name)

	@classmethod
	def find(cls,ruc):
		customers = cls.objects.filter(
			ruc=ruc)
		if customers.exists():
			return customers[0]
		return None
		
		
		