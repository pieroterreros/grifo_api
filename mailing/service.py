from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string


def send_email_message(subject, from_email, to_email, template_html, template_txt, context=None):
    ctx = context or {}

    text_content = render_to_string(template_txt, ctx)
    html_content = render_to_string(template_html, ctx)

    message = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    message.attach_alternative(html_content, "text/html")

    message.send()

    return  {"send_to": to_email}