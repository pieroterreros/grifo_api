# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory, RequestsClient
import json


# Create your tests here.
class TestSaleApi(TestCase):
    def setUp(self):
        client = RequestsClient()
        request = client.post(
            'http://localhost:8000/api/sale/', json={  
                        "customer":{
                            "first_name":"gian",
                            "last_name":"burga",
                            "ruc":"1243823",
                            "email":"gian.burga@gmail.com",
                            "address":"escardo 529"
                        },
                        "igv":18,
                        "total":29.8,
                        "sale_type":"cash",
                        "invoice_type":"boleta_venta",
                        "series":"B012",
                        "sale_date":"2014-04-12",
                        "expire_date":"2014-04-12",
                        "identifier":"00000234",
                        "subtotal":"20",
                        "extras":"",
                        "detail":[
                            {
                                "product":{
                                    "price":12.3,
                                    "code":"ED32",
                                    "name":"GS90",
                                    "measure":"GLN"
                                },
                                "discounts":0,
                                "price":24.6,
                                "quantity":2,
                                "measure":"GLN"
                                
                            }]
                        }, headers={'content_type':'application/json',
                        'authorization':'bearer Goet7tOAXurteSY0zJFTodlYhinLs7'})
        # print request.status_code
        # assert request.status_code == 201
        assert request.status_code == 500
