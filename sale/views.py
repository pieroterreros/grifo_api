# -*- coding: utf-8 -*-
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from customer.utils import get_access_token
from .models import Sale
from .serializers import SaleSerializers, SaleAllSerializers, SaleDetalSerializer
from customer.serializers import CustomerSerializers
from customer.models import Customer
from product.models import Product
from product.serializers import ProductSerializers


# Create your views here.

@api_view(['POST', 'PUT', 'GET'])
@permission_classes([AllowAny])
def SaleView(request):
    request_data = request.data
    if request.method == 'POST':
        try:
            company = get_access_token(request).user.user_company
            # se valida el json del sale
            sale_serilizers = SaleSerializers(data=request_data)
            if not sale_serilizers.is_valid():
                return Response(sale_serilizers.errors, status=400)

            # se valida el json del customer
            customer = request_data.get('customer')
            customer_serializer = CustomerSerializers(data=customer)
            if not customer_serializer.is_valid():
                return Response(customer_serializer.errors, status=400)

            # buscamos si e customer ya se encuentra registrado
            customer = Customer.find(customer.get('ruc'))
            if not customer:
                customer = customer_serializer.save()

            try:
                sale = Sale.find(
                    company.ruc,
                    request_data.get('invoice_type'),
                    '%s-%s' % (request_data.get('series'),request_data.get('identifier')),
                    request_data.get('sale_date'),
                    request_data.get('total'))
                return Response({"detail":"The sale exist"})
            except Sale.DoesNotExist as e:
                print str(e)
            # se crea la venta
            sale = sale_serilizers.save(customer=customer, company=company)
            sale.send_email()

            details = request_data.get('detail')
            if details:
                for detail in details:
                    # validamos que el producto exista
                    product = detail.get('product')
                    product_serializer = ProductSerializers(data=product)
                    if not product_serializer.is_valid():
                        return Response(product_serializer.errors, status=400)

                    product = Product.find(product.get('code'))
                    if not product:
                        product = product_serializer.save()

                    # creamos el detalle
                    sale_detail_serializer = SaleDetalSerializer(data=detail)
                    if not sale_detail_serializer.is_valid():
                        return Response(sale_detail_serializer.errors, status=400)

                    sale_detail_serializer.save(product=product,
                                                sale=sale)
            else:
                return Response({'detail': ['required field']}, status=400)

            return Response(SaleAllSerializers(sale, many=False).data,status=201)
        except Exception as e:
            return Response({
                "detail": str(e)},
                status=401)

    if request.method == 'GET':
        ruc = request.GET.get('ruc')
        invoice_type = request.GET.get('invoice_type')
        identifier = request.GET.get('identifier')
        sale_date = request.GET.get('sale_date')
        total = request.GET.get('total')

        if not ruc or not invoice_type or not identifier or not sale_date or not total:
            return response({"detail":"bad request"}, status=400)
        try:
            sale = Sale.find(ruc,invoice_type,identifier,sale_date,total)
            return Response(SaleAllSerializers(sale, many=False).data)
        except Sale.DoesNotExist as e:
            return Response({"detail":"The sale does not exist"},status=404)

    if request.method == 'PUT':
        pass

    
