# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Sale, SaleDetail

# Register your models here.

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
	list_display = ('uuid', 'series', 'identifier', 'invoice_type', 
					'total', 'sale_date', 'customer')
	search_fields = ['uuid', 'series', 'identifier', 'total', 
					'sale_date', 'customer__person__first_name']
