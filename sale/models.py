# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from django.db import models
from customer.models import Customer
from company.models import Company
from product.models import Product
from decimal import Decimal
from .tasks import send_email_tasks


# Create your models here.

class Sale(models.Model):
    SALE_TYPE_CREDIT = 'credit'
    SALE_TYPE_CASH = 'cash'
    SALE_TYPE_CONSUMO_INTERNO = 'consumo_interno'
    SALE_TYPE_SERAFIN = 'serafin'
    SALE_TYPE_NO_INVOICE = 'no_invoice'

    SALE_TYPES = (
        (SALE_TYPE_CREDIT, u'Crédito'),
        (SALE_TYPE_CASH, 'Al Contado'),
        (SALE_TYPE_NO_INVOICE, 'Sin comprobante'),
        (SALE_TYPE_CONSUMO_INTERNO, 'Consumo Interno'),
        (SALE_TYPE_SERAFIN, 'Serafin'),
    )

    INVOICE_FACTURA = 'factura'
    INVOICE_BOLETA_VENTA = 'boleta_venta'
    INVOICE_NOTA_CREDITO = 'nota_credito'

    INVOICE_TYPES = (
        (INVOICE_FACTURA, 'Factura'),
        (INVOICE_BOLETA_VENTA, 'Boleta de venta'),
        (INVOICE_NOTA_CREDITO, u'Nota de crédito')
    )


    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    sale_type = models.CharField(choices=SALE_TYPES,max_length=25)
    identifier = models.CharField(max_length=8)
    series = models.CharField(max_length=4)
    invoice_type = models.CharField(choices=INVOICE_TYPES,max_length=25)
    sale_date = models.DateField()
    expire_date = models.DateField()
    subtotal = models.DecimalField(max_digits=19, decimal_places=2)
    total = models.DecimalField(max_digits=19, decimal_places=2)
    igv = models.IntegerField()
    extras = models.TextField(max_length=500, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(blank=True, null=True)
    synced = models.DateField(blank=True, null=True)
    customer = models.ForeignKey(Customer)
    company = models.ForeignKey(Company)


    """docstring for Sale"""

    class Meta:
        verbose_name = u"Sale"
        verbose_name_plural = "Sales"

    def __unicode__(self):
        return self.identifier

    @classmethod
    def find(cls,ruc,invoice_type,identifier,sale_date,total):
        identifier = identifier.split('-')
        sale = cls.objects.get(
            company__ruc=ruc,invoice_type=invoice_type,
            series=identifier[0],identifier=identifier[1],
            sale_date__icontains=sale_date)
        
        return sale

    def send_email(self, to_email=None):
        send_email_tasks.delay(self)

class SaleDetail(models.Model):
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    measure = models.CharField(max_length=50)
    quantity = models.DecimalField(max_digits=19, decimal_places=2)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    discounts = models.DecimalField(max_digits=19, decimal_places=2)
    product = models.ForeignKey(Product)
    sale = models.ForeignKey(Sale, related_name='detail')

    """docstring for SaleDetail"""

    class Meta:
        verbose_name = u"Sale detail"
        verbose_name_plural = "Sale details"

    def __unicode__(self):
        return  self.sale




