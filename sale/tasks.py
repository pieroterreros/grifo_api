# -*- coding: utf-8 -*-
from grifo_api.celery import app
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from mailing.service import send_email_message

@app.task
def send_email_tasks(self, to_email=None):
    if to_email or self.customer.email:
            subject = u'Comprobante de Pago electrónico %s # %s-%s' % (
                self.get_invoice_type_display(),
                self.series,
                self.identifier,
            )
            from_email = 'no-reply@peruinvoices.com'
            to_email = to_email or self.customer.email

            try:
                validate_email(to_email)
            except ValidationError:
                return

            template_html = 'email.html'
            template_txt = 'email.txt'
            context = {
                'sale': self,
                'link_url': 'http://labceperu.com/grifo/',
                'link_name': 'peruinvoices.com'
            }
            send_email_message(
                subject,
                from_email,
                to_email,
                template_html,
                template_txt,
                context=context
            )