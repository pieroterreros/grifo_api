# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Sale, SaleDetail
from customer.serializers import CustomerSerializers
from company.serializers import CompanySerializers
from product.serializers import ProductSerializers


class SaleSerializers(serializers.ModelSerializer):
    company = serializers.CharField(
        max_length=50, required=False)

    class Meta:
        model = Sale
        exclude = ['customer']


class SaleDetalAllSerializer(serializers.ModelSerializer):
    product = ProductSerializers()

    class Meta:
        model = SaleDetail
        exclude = ['sale']


class SaleAllSerializers(serializers.ModelSerializer):
    customer = CustomerSerializers()
    company = CompanySerializers()
    detail = SaleDetalAllSerializer(many=True, read_only=True)

    class Meta:
        model = Sale
        fields = '__all__'


class SaleDetalSerializer(serializers.ModelSerializer):
    sale = serializers.CharField(
        max_length=50, required=False)

    class Meta:
        model = SaleDetail
        exclude = ['product']
