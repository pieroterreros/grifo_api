# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Company

# Register your models here.


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'ruc', 'busisness_name',
                    'telephone', 'address', 'user',)
    search_fields = ['uuid', 'ruc', 'busisness_name', 'telephone',
                     'address', 'person__first_name', 'user__last_name', ]
    list_filter = ('busisness_name',)
