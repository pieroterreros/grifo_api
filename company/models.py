# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Company(models.Model):
	uuid = models.UUIDField(
		primary_key=True, default=uuid.uuid4, editable=False)
	ruc = models.CharField(max_length=11)
	busisness_name = models.CharField(max_length=100)
	telephone = models.CharField(max_length=15)
	address = models.CharField(max_length=200)
	user = models.OneToOneField(User, null=True, blank=True, related_name='user_company')
	"""docstring for """

	class Meta:
		verbose_name = u"Company"
		verbose_name_plural = "Companies"

	def __unicode__(self):
		return self.busisness_name
